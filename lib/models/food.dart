part of 'models.dart';

enum FoodType { new_food, popular, recommended }

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;
  final List<FoodType> types;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate,
      this.types = const []});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, picturePath, description, ingredients, price, rate];
}

List<Food> mockFoods = [
  Food(
       id: 1,
      picturePath: "assets/food/salad_one.jpg",
      name: "Salad Buah Size 500ml",
      description: "Salad Buah topping keju",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 25000,
      rate: 4.6),
  Food(
      id: 2,
      picturePath: "assets/food/s2.jpg",
      name: "Salad Buah Size 500ml",
      description: "Salad Buah topping mix",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 30000,
      rate: 4.5),
  Food(
      id: 3,
      picturePath: "assets/food/s3.jpg",
      name: "Salad Buah Size 450ml",
      description: "Salad Buah topping keju",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 20000,
      rate: 4.5),
  Food(
      id: 4,
      picturePath: "assets/food/s4.jpg",
      name: "Salad Buah Size 450ml",
      description: "Salad Buah topping mix",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 25000,
      rate: 4.5),
  Food(
      id: 5,
      picturePath: "assets/food/s5.jpg",
      name: "Salad Buah Size 400ml",
      description: "Salad Buah topping keju",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 15000,
      rate: 4.4),
  Food(
      id: 6,
      picturePath: "assets/food/s6.jpg",
      name: "Salad Buah Size 400ml",
      description: "Salad Buah topping mix",
      ingredients: "buah : Apel, Melon, Semangka, Naga, Anggur, Strawberry, Jelly",
      price: 20000,
      rate: 4.5,
  ),
];
